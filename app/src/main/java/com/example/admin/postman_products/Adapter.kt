package com.example.admin.postman_products

/**
 * Created by Admin on 11-03-2018.
 */
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_layout.view.*

class Adapter(val feed:Feed,val context: Context) :RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
        val cellforrow = v.inflate(R.layout.list_layout, parent, false)
        return ViewHolder(cellforrow)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val products = feed.data.products.get(position)
        holder?.itemView?.product_id?.text = products.product_id.toString()
        holder?.itemView?.product_name?.text = products.product_name
        holder?.itemView?.product_desc?.text = products.product_desc
        holder?.itemView?.highest_bid?.text = products.highest_bid.toString()
        holder?.itemView?.delivered_on_date?.text = products.delivered_on_date.toString()
        holder?.itemView?.receiver_user_id?.text = products.receiver_user_id.toString()
        holder?.itemView?.is_not_abuse?.text = products.is_not_abuse.toString()
        holder?.itemView?.bid_expiry_date?.text = products.bid_expiry_date
        holder?.itemView?.less_than_one_day?.text = products.less_than_one_day.toString()
        var url_image=products.images
       Glide.with(context).load(url_image).into(holder?.itemView?.images)
      /* Thread(Runnable {

           this@Adapter.run{
               try {
                   val image=BitmapFactory.decodeStream(url_image.openConnection().getInputStream())
                   holder?.itemView?.images?.setImageBitmap(image)
               }
              catch ( e:NumberFormatException){
                  null
              }
           }

       }).start()
*/

    }
        //this method is giving the size of the list
        override fun getItemCount(): Int {
            return feed.data.products.size
        }
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }