package com.example.admin.postman_products

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class ShowProducts : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_products)

    }
    fun show(view: View)
    {
        val intent=Intent(this,DisplayActivity::class.java)
        startActivity(intent)
    }
}
