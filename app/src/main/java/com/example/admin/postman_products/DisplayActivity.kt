package com.example.admin.postman_products

import android.app.ProgressDialog
import android.media.Image
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout

import kotlinx.android.synthetic.main.list_activity.*
import okhttp3.*
import java.io.IOException
import com.google.gson.GsonBuilder
import okhttp3.RequestBody
import okhttp3.OkHttpClient
import java.net.URI
import java.net.URL


class DisplayActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_activity)
        val recyclerView = findViewById(R.id.recyclerView) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        fetchJson()
    }
  fun fetchJson()
  {
     val client = OkHttpClient()

      val mediaType = MediaType.parse("application/octet-stream")
      val body = RequestBody.create(mediaType, "{\n\"url\":\"https://www.gstatic.com/webp/gallery3/2.png\"\n}\n\n")
      val request = Request.Builder()
              .url("http://139.162.7.89/my_products/3/")
              .get()
              .addHeader("Authorization", "oauth feb1ee0dd94b712f91f2b053dca1f862")
              .addHeader("Cache-Control", "no-cache")
              .addHeader("Postman-Token", "a38050f6-0074-4ea9-8315-931806a9a62f")
              .build()
    client.newCall(request).enqueue(object :Callback{
        override fun onFailure(call: Call?, e: IOException?){
            println("failed to display")
        }

        override fun onResponse(call: Call?, response: Response?) {
            val body=response?.body()?.string()
            println(body)
            val gson=GsonBuilder().create()
           val feed=gson.fromJson(body,Feed::class.java)
          runOnUiThread {
               recyclerView.adapter = Adapter(feed,this@DisplayActivity)
           }
        }

    })

  }
}

//class Feed(val success:Boolean)
class Feed(val data:products)
class products(val products:List<Product>)
class Product(val product_id:Int,val product_name:String,val product_desc:String,val highest_bid:Int,val delivered_on_date:Int,val receiver_user_id:Int,val is_not_abuse:Int,
              val bid_expiry_date:String,val less_than_one_day:Int,var images:URL )

